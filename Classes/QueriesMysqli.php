<?php

class QueriesMysqli{

    protected $db;

    function __construct($conn)
    {
        $this->db = $conn;
    }

    public function query($sql){
      return $this->db->query($sql);
    }

}