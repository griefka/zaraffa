<?php


class DbConnection{

        protected $db;


        public function connect($type, $host, $db_name, $user, $password){

            if($type == 'PDO') {

                try {
                    $db = new PDO('mysql:host=' . $host . ';dbname=' . $db_name, $user, $password);
                } catch (PDOException $e) {
                    echo $e->getMessage();
                }
            }

            if($type == 'mysqli'){

                $db = new mysqli($host, $user, $password, $db_name);

             if ($db->connect_errno) {
               echo "Не удалось подключиться к MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
              }
//            echo $db->host_info . "\n";
            }
            return $db;
        }

}