<?php

class Queries{

    protected $db;

    function __construct($conn) {
        $this->db = $conn;
    }

    public function findAll(){
        $query = $this->db->prepare("SELECT * FROM users");
        $query->execute();
        return $query->fetchAll();
    }

    public function findByPk($id){
        $query = $this->db->prepare("SELECT * FROM users WHERE id=:id");
        $query->bindParam(':id', $id);
        $query->execute();
        return $query->fetchAll();
    }

    public function sql($sql){
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $sth = $this->db->query($sql);
        $arr = array();
        while ($row = $sth->fetchAll(PDO::FETCH_ASSOC)) {
            array_push($arr, $row);
//            array_push($arr,$row['login'].$row['password'].$row['id']);
        }
        return $arr;
    }



}