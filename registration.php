<?php
$db = new PDO('mysql:host=localhost;dbname=cats', 'root', '1111');

if(isset($_POST)){

        $query = $db->prepare('INSERT INTO users (login, email, first_name, last_name, password)
        VALUES (:login, :email, :first_name, :last_name, :password)');

        $query->bindParam(':login', $login);
        $query->bindParam(':email', $email);
        $query->bindParam(':password', $password);
        $query->bindParam(':first_name', $first_name);
        $query->bindParam(':last_name', $last_name);


        $login = htmlentities($_POST['login'],ENT_QUOTES,'UTF-8');
        $email = htmlentities($_POST['email'],ENT_QUOTES,'UTF-8');
        $first_name = htmlentities($_POST['first_name'],ENT_QUOTES,'UTF-8');
        $last_name = htmlentities($_POST['last_name'],ENT_QUOTES,'UTF-8');
        $password = htmlentities($_POST['password'],ENT_QUOTES,'UTF-8');


         $error_array = array('url'=>'', 'errorMessage'=>array());

         if(strlen($login)<3)
         {
             array_push($error_array['errorMessage'], 'login');
         }
          if (!filter_var($email, FILTER_VALIDATE_EMAIL))
         {
            array_push($error_array['errorMessage'], 'email');
         }
        if(empty($error_array['errorMessage'])){
            $query->execute();
            $login_page = "http://" . $_SERVER['SERVER_NAME'].'/login_view.php';
            $error_array['url'] = $login_page;
        }
            echo json_encode($error_array);
}




