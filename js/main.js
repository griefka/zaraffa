$(document).ready(function(){

    $('#register-button').on('click', function(e){
        window.location.href = '../registration_view.php';
    });

    $('#login-submit').on('click', function(e){
        e.preventDefault();

        var form = $('#login-form');

        $.ajax({
            type: 'POST',
            url: 'login.php',
            data: form.serialize(),
            success: function(){
                alert('hi');
            }
        })
    });

    $('#reg-submit').on('click', function(e){
        e.preventDefault();

        var form = $('#registration-form');

        $.ajax({
            type: 'POST',
            url: "registration.php",
            data: form.serialize(),
            success: function(data){

                $('.warning:input').removeClass('warning');
                  var request = JSON.parse(data);

                if(request['errorMessage'].length > 0){
                    $.each(request['errorMessage'], function(i, val)
                    {
                        $('input[name='+val+']').addClass('warning');
                    });
                }
                if(request['url'].length!=0)
                {
                    window.location.href = request['url'];
                }

//todo: сделать редирект  и

                    //$.each(request, function(i, val)
                    //{
                    //$('input[name='+val+"]").addClass('warning');
                    //});
            }
        });

    });
});