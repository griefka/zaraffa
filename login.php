<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Логин в сеть котиков</title>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="/js/main.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/styles.css" media="screen" />
</head>
<body>

<div id="form">
<form method="post" action="login_main.php" id="login-form">
    <input name="login" type="text" placeholder="Login">
    <input name="password" type="password" placeholder="Password">
    <input type="submit" value="Login" name="form" id="login-submit">
    <button name="register" type="button" id="register-button">Register</button>
</form>
    <div id="error"></div>
</div>

</body>
</html>